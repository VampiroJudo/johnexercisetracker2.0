import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navbar from './components/navbar.js';
import ExercisesList from './components/exercises-list.js';
import EditExercise from './components/edit-exercise.js';
import CreateExercise from './components/create-exercise.js';
import CreateUser from './components/create-user.js';


import './App.css';

function App() {
  return (
    <Router>
      <div className="container">
        <Navbar />
        <br/> 
        <Route path='/' exact component={ExercisesList} />
        <Route path='/edit/:id' exact component={EditExercise} />
        <Route path='/create' exact component={CreateExercise} />
        <Route path='/user' exact component={CreateUser} />
      </div>
    </Router>
  );
}

export default App;
