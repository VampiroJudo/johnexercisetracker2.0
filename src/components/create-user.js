import React, { Component } from 'react';
import Axios from 'axios';

export default class CreateUsers extends Component {
	constructor(props) {
		super(props)

		this.onChangeUsername = this.onChangeUsername.bind(this);
		this.onChangeUserAge = this.onChangeUserAge.bind(this);
		this.onChangeUserHeight = this.onChangeUserHeight.bind(this);
		this.onChangeUserWeight = this.onChangeUserWeight.bind(this);
		this.onSubmit = this.onSubmit.bind(this);

		this.state = {
			username: '',
			age: 0,
			height: '',
			weight: 0
		}
	}

	onChangeUsername(e) {
	  this.setState({
	  	username: e.target.value
	  });
	}

	onChangeUserAge(e) {
	  this.setState({
	  	age: e.target.value
	  });
	}

	onChangeUserHeight(e) {
	  this.setState({
	  	height: e.target.value
	  });
	}

	onChangeUserWeight(e) {
	  this.setState({
	  	weight: e.target.value
	  });
	}

	onSubmit(e) {
	  e.preventDefault();
	  
	  const user = {
		username: this.state.username,
		age: this.state.age,
		height: this.state.height,
		weight: this.state.weight,
	  }
	  console.log(user)

	  Axios.post('http://localhost:5000/users/add', user)
		.then(res => console.log(res.data));

	  this.setState({
		username: '',
		age: '',
		height: '',
		weight: '',
	  });
	}

	render() {
	  return (
		<div>
		  <h3>Create New User</h3>
		  <form onSubmit={this.onSubmit}>
			<div className="form-group">
			  <label>Username: </label>
			  <input type="text" required className="form-control" value={this.state.username} onChange={this.onChangeUsername}/>
			</div>
			<div className="form-group">
			  <label>Age: </label>
			  <input type="text" required className="form-control" value={this.state.age} onChange={this.onChangeUserAge}/>
			</div>
			<div className="form-group">
			  <label>Height (in feet and inches): </label>
			  <input type="text" required className="form-control" value={this.state.height} onChange={this.onChangeUserHeight}/>
			</div>
			<div className="form-group">
			  <label>Weight: </label>
			  <input type="text" required className="form-control" value={this.state.weight} onChange={this.onChangeUserWeight}/>
			</div>
			<div className="form-group">
			  <input type="submit" value="Create User" className="btn btn-primary" />
			</div>
		  </form>
		</div>
	  );
	}
}